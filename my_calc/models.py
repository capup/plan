#from datetime import datetime
from django.db import models
from django.utils import timezone


class Event(models.Model):
    TYPE_STATUS = (
        ('actually', 'Актуально'),
        ('canceled', 'Відмінено'),
        ('not_specified', 'Не визначено')
    )

    TYPE_EVENT = (
        ('event', 'Захід'),
        ('methodical_work', 'Методична робота'),
        ('sports', 'Спортивне змагання'),
        ('exhibition', 'Виставка'),
        ('rehearsal', 'Репетиція')
    )

    TYPE_DIRECT = (
        ('bibliographic', 'Бiблiотечно - бiблiографiчний'),
        ('vіyskovo_patriotic', 'Військово - патріотичний'),
        ('humanitarian', 'Гуманітарний'),
        ('doslidnitsko_experimental', 'Дослідницько - експериментальний'),
        ('ecological_natural', 'Еколого - натуралістичний'),
        ('mainly_trenuvalny_zbіr', 'Навчально - тренувальний збір'),
        ('naukovo_technical', 'Науково - технічний'),
        ('national_patriotic', 'Національно - патріотичний'),
        ('wellness', 'Оздоровчий'),
        ('socially_rehabilitation', 'Соціально - реабiлiтацiйний'),
        ('turistsko_kraznavchiy', 'Туристсько - краєзнавчий'),
        ('culturally_sports', 'Фізкультурно - спортивний або спортивний'),
        ('hudozhno_aesthetic', 'Художньо - естетичний')

    )

    DEPARTMENT = (
        ('Авіаційно-космічний центр', 'Авіаційно-космічний центр'),
        ('Відділ біології', 'Відділ біології'),
        ('Відділ народної творчості', 'Відділ народної творчості'),
        ('Відділ соціальних ініціатив і партнерства', 'Відділ соціальних ініціатив і партнерства'),
        ('Відділ туризму та краєзнавства', 'Відділ туризму та краєзнавства'),
        ('Відділ художньої творчості', 'Відділ художньої творчості'),
        ('Відділ івент-менеджменту та арт-проектів', 'Відділ івент-менеджменту та арт-проектів'),
        ('Навчально-методичний відділ', 'Навчально-методичний відділ'),
        ('Спортивний відділ', 'Спортивний відділ'),
        ('Інформаційно-творче агентство “ЮН-ПРЕС”', 'Інформаційно-творче агентство “ЮН-ПРЕС”'),
        ('Відділ STEM-освіти', 'Відділ STEM-освіти'),

    )

    LEVEL = (
        ('Палацу', 'Палацу'),
        ('Міський', 'Міський'),

        ('Всеукраїнський', 'Всеукраїнський'),
        ('Міжнародний', 'Міжнародний'),

    )
    ENGAGEMENT = (
        ('Організація', 'Організація'),
        ('Участь', 'Участь'),
        ('Допомога в організації', 'Допомога в організації'),
    )

    PLAN_UNPLAN = (
        ('Плановий', 'Плановий'),
        ('Позаплановий', 'Позаплановий'),
    )

    MONTHS = [
        'Січень',
        'Лютий',
        'Березень',
        'Квітень',
        'Травень',
        'Червень',
        'Липень',
        'Серпень',
        'Вересень',
        'Жовтень',
        'Листопад',
        'Грудень'
    ]

    today_tmp = timezone.now()  # str(datetime.now().strftime('%Y-%m-%d'))
    time_tmp = '10:00'
    month_tmp = str(MONTHS[int(timezone.now().strftime("%m")) - 1])  #
    # print(month_tmp)

    month = models.CharField('Місяць', max_length=255, default=month_tmp, blank=True, null=True)
    name_event = models.CharField('Назва', max_length=255, default='',help_text='Назва заходу', blank=False, null=False)
    status = models.CharField('Статус', max_length=255, choices=TYPE_STATUS, default=TYPE_STATUS[0], blank=True,
                              null=False)
    day_start = models.DateField('Дата початку', default=today_tmp, blank=True, null=True)
    time_start = models.TimeField('Час початку', default=time_tmp, blank=True, null=True)

    day_end = models.DateField('Дата закінчення', default=today_tmp, blank=False)
    time_end = models.TimeField('Час закінчення', default=time_tmp, blank=False)

    place_event = models.CharField('Місце', max_length=255, default='КПДЮ', blank=True, null=True)
    type_event = models.CharField('Тип', max_length=255, default=TYPE_EVENT[0], choices=TYPE_EVENT, blank=True,
                                  null=True)
    direct_event = models.CharField('Напрям', max_length=255, choices=TYPE_DIRECT, default=TYPE_DIRECT[0], blank=True,
                                    null=True)
    department = models.CharField('Відділ', max_length=255, choices=DEPARTMENT, default='', blank=True, null=True)
    partner = models.CharField("Партнери", max_length=255, default='', blank=True, null=True)
    responsible_person = models.CharField("Відповідальний", max_length=255, default='', blank=True, null=True)
    national_patriotic_event = models.BooleanField('Національно-патриотичне виховання',
                                                   default=False,
                                                   blank=True,
                                                   null=False)
    imap_department_aid = models.BooleanField('Допомога ІМАП',
                                              default=False,
                                              blank=True,
                                              null=False)
    vxt_department_aid = models.BooleanField('Допомога ВХТ',
                                             default=False,
                                             blank=True,
                                             null=False)
    music_accompaniment = models.BooleanField('Музичний супровід', default=False, blank=True, null=False)
    target_audience = models.CharField("Цільова аудиторія", max_length=255, default='', blank=True, null=True)
    number_of_participants = models.IntegerField("Кількість учасників", default=1, blank=True, null=True)
    achievement = models.TextField('Досягнення', max_length=255, default='', blank=True, null=True)
    level = models.CharField('Рівень', max_length=255, choices=LEVEL, default=LEVEL[0], blank=True, null=True)
    engagement = models.CharField('Роль', max_length=255, choices=ENGAGEMENT, default=ENGAGEMENT[0], blank=True,
                                  null=True)
    plan_unplan = models.CharField('Плановий', max_length=255, choices=PLAN_UNPLAN, default=PLAN_UNPLAN[0], blank=True,
                                   null=False)
    created_by = models.CharField('Створив', max_length=255, default="", blank=True, null=True)
    created_date = models.DateField('Дата створення', default=today_tmp, blank=True, null=True)
    last_edition_date = models.DateField('Дата останньої редакції', default=today_tmp, blank=True, null=True)

    def __str__(self):
        return ('{}\t{}\t{}').format(self.day_start, self.name_event, self.department)


class Users(models.Model):
    DEPARTMENT = Event.DEPARTMENT
    USER_GROUP =(
        ('Перегляд', 'Перегляд'),                       # за вмовченням
        ('Редактор', 'Редактор'),                       # вносить та редактує тільки свої записи
        ('Адміністратор', 'Адміністратор'),             # редактує всі записи відділу
        ('Суперадміністратор', 'Суперадміністратор'),   # редактує всі записи
        ('Звіт', 'Звіт'),                               # спецрежим
    )

    today_tmp = timezone.now()

    name = models.CharField('І`мя', max_length=255, default='', blank=True, null=True)
    surname = models.CharField('Прізвище', max_length=255, default='', blank=True, null=True)
    phone = models.CharField('Телефон', max_length=255, default='', blank=True, null=True)
    email = models.CharField('email', max_length=255, default='', blank=True, null=True)
    login = models.CharField('Логін', max_length=255, default='', blank=True, null=True)
    password = models.CharField('Пароль', max_length=255, default='', blank=True, null=True)
    department = models.CharField('Відділ', choices=DEPARTMENT, max_length=255, default="", blank=True, null=True)
    date_registration = models.DateTimeField('Дата реєстрації', default=today_tmp, blank=True, null=True)
    date_last_enter = models.DateTimeField('Останній вхід', default=today_tmp, blank=True, null=True)
    user_group = models.CharField('Відділ', choices=USER_GROUP, max_length=255, default=USER_GROUP[0], blank=True, null=False)
    def __str__(self):  # виводить в адмінку гарні рядочки
        return ('{} {} - {}').format(self.name, self.surname, self.department)


class History(models.Model):
    data_time = models.DateTimeField('Дата', default=timezone.now(), blank=True, null=True)
    login = models.CharField('Логін', max_length=255, default='', blank=True, null=True)
    activity = models.CharField('Дія', max_length=255, default='', blank=True, null=True)

    def __str__(self):  # виводить в адмінку гарні рядочки
        return ('{} {} - {}').format(self.data_time, self.login, self.activity)
