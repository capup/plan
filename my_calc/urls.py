from django.urls import path, include
from . import views as function
urlpatterns = [
    path('', function.table_page),
    path('table', function.table_page,name='table'),
    path('register', function.register_page,name='register'),
    path('enter', function.enter,name='enter' ),
    path('test', function.test),
    path('table_form', function.table_form,name='table_form'),
    path('exit', function.exit,name='exit'),
    path('cabinet', function.cabinet,name='cabinet'),
    path('add_event', function.add_event,name='add_event'),
    path('event', function.event,name='event')
    #path('', Blog.as_view(), name='blog'),
]