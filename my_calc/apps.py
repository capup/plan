from django.apps import AppConfig


class MyCalcConfig(AppConfig):
    name = 'my_calc'
