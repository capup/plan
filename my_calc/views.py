import hashlib
from time import timezone

from django.forms.utils import ErrorList
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render



from .forms import RegistrationForm, EventForm
from django.core.exceptions import *
from django.shortcuts import redirect
from django.shortcuts import render_to_response
from django.template import RequestContext

from .models import Event
from .models import Users




def table_form(request):
    ctx = {}
    return render(request, 'table_form.html')


def test(request):
    ctx = {}
    ctx['form'] = RegistrationForm
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
    return render(request, 'test.html', ctx)


def register_page(request):
    DEPARTMENT_REG = {'DEPARTMENT_REG': [d[1] for d in Users.DEPARTMENT]}
    ctx = {}
    ctx.update(DEPARTMENT_REG)
    if request.method == 'GET':
        return render(request, 'register.html', ctx)
    if request.method == 'POST' and request.POST.get('submit') == 'Send':
        try:
            name = request.POST.get('name')
            surname = request.POST.get('surname')
            phone = request.POST.get('phone')
            email = request.POST.get('email')
            login = request.POST.get('login')
            password1 = request.POST.get('password1')
            password2 = request.POST.get('password2')
            department = request.POST.get('department')
            #date_registration = timezone.now()
            ctx.update({'name': name,
                        'surname': surname,
                        'phone': phone,
                        'email': email,
                        'login': login,
                        'department': department})
            if name == '' or name is None or \
                    surname == '' or surname is None or \
                    phone == '' or phone is None or \
                    email == '' or email is None or \
                    login == '' or login is None or \
                    password1 == '' or password1 is None or \
                    department == '' or department is None:
                print('Існують незаповнені поля!')
                raise ValueError('Існують незаповнені поля!')
            elif Users.objects.filter(name=name, surname=surname).exists():
                print('Користувач з таким ім`ям та прізвищем вже зареєстровано!')
                raise ValueError("Користувач з таким ім`ям та прізвищем вже зареєстровано!")
            elif Users.objects.filter(phone=phone).exists():
                print('error - 3')
                raise ValueError("Такий номер вже зареєстровано!")
            elif Users.objects.filter(login=login).exists():
                print('Такий номер вже зареєстровано!')
                raise ValueError("Такий логін вже зареєстровано!")
            elif password1 != password2 and password1 is not '' and password1 is not None:
                print('Підтвердження паролю не співпадає!')
                raise ValueError('Підтвердження паролю не співпадає!')
            else:
                if password1 is not None or password1 is not '':
                    password1 = (hashlib.md5(password1.encode())).hexdigest()
                else:
                    pass
                Users.objects.create(name=name,
                                     surname=surname,
                                     phone=phone,
                                     email=email,
                                     login=login,
                                     password=password1,
                                     department=department
                                     )

                request.session['user'] = login
                print('Успех')

        except Exception as error:
            ctx.update({'main_error': error})
            return render(request, 'register.html', ctx)
        return redirect('enter')


def enter(request):
    ctx = {}
    if request.method == 'POST' and request.POST.get('submit') == 'Enter':
        login = request.POST.get('login')
        password = request.POST.get('password')
        global current_login
        current_login = login
        ctx.update({'login': login})
        print(login, password)
        if password is not None or password is not '':
            password = (hashlib.md5(password.encode())).hexdigest()
        if Users.objects.filter(login=login, password=password).exists():
            response = redirect('table')
            response.set_cookie('login', login)
            return response
        else:
            error = 'Помилка логіну або паролю!'
            ctx.update({'main_error': error})
    return render(request, 'enter.html', ctx)


def home_page(request):
    return render(request, 'table.html', {})


def table_page(request):
    ctx = {}
    all_events = Event.objects.all().order_by('day_start')
    # all_events = Event.objects.filter(is_event_name = True, event_name_contains = 'Чемпионат')
    # print(all_events)
    # for event in all_events:
    # print(event.name_event)
    ctx['events'] = all_events
    return render(request, 'table.html', ctx)


def exit(request):
    global current_login
    current_login = None
    response = redirect('table')
    response.delete_cookie('login')
    return response


def cabinet(request):
    ctx = {}
    global current_login
    print('login: ', current_login)
    # user = Users.objects.filter(login="qqq")
    user = Users.objects.filter(login=current_login)
    ctx['user'] = user
    print(ctx)
    if request.method == 'POST' and request.POST.get('exit_cabinet') == 'Ok':
        return redirect('table')
    return render(request, 'cabinet.html', ctx)


def add_event(request):
    ctx = {}
    ctx['form'] = EventForm

    # print (EventForm.get_all_field_names())   ############
    if request.method == 'GET':
        return render(request, 'add_event.html', ctx)

    if request.method == 'POST' and request.POST.get('add_event_btn') == 'AddEvent':
        form = EventForm(request.POST)
        fields_d = {}
        try:

            for field in form.fields:
                fields_d.update({field: request.POST.get(field)})
                #print(field, fields[field])
                #print('fields_d',fields_d)
            name_event = request.POST.get('name_event')
            partner = request.POST.get('partner')
            print ('name_event: ',name_event)





            if name_event == '' or name_event is None :
                print('Назва заходу не вказана!')
                raise ValueError('Назва заходу не вказана!')
            elif partner == '' or partner is None :
                print('Вкажіть партнерів!')
                raise ValueError('Вкажіть партнерів!')
            elif Event.objects.filter(name_event=name_event).exists():
                print('Захід існує!')
                raise ValueError("Захід існує!")
            else:
                if 1==1:
                    pass
                else:
                    pass
                form.save()
                print('Успех')

        except Exception as error:
            ctx['main_error']= error
            return render(request, 'add_event.html', ctx)

    elif request.method == 'POST' and request.POST.get('cancel_btn') == 'Cancel':
        return redirect("table")
    elif current_login == None:
        return redirect("enter")

    return render(request, 'add_event.html', ctx)


def edit_event(request):
    ctx = {}
    return render(request, 'table.html', ctx)

def event(request):
    ctx = {}
    if request.method == 'GET':
        ctx['form'] = EventForm()
        return render(request, 'event.html', ctx)
    elif request.method == 'POST':
        form = EventForm(request.POST)
        if Event.objects.filter(name_event=request.POST.get('name_event')).exists():
            #EventForm.add_error('name_event','Такий існує!')
            form._errors['name_event'] = ErrorList(["!!!!"])


            ctx['form'] = EventForm(request.POST)
            return render(request, 'event.html', ctx)
        elif form.is_valid():
            form.save()
            #username = form.cleaned_data['username']
            #password = form.cleaned_data['password1']
            #user = authenticate(username=username, password=password)
            #login(request, user)
            return redirect('table')
        else:
            pass



"""
name_event = request.POST.get('name_event')
status = request.POST.get('status')
day_start = request.POST.get('day_start')
time_start = request.POST.get('time_start')
day_end = request.POST.get('day_end')
time_end = request.POST.get('time_end')
place_event = request.POST.get('place_event')
type_event = request.POST.get('type_event')
direct_event = request.POST.get('direct_event')
department = request.POST.get('department')
partner = request.POST.get('partner')
responsible_person = request.POST.get('responsible_person')
national_patriotic_event = request.POST.get('national_patriotic_event')
imap_department_aid = request.POST.get('imap_department_aid')
vxt_department_aid = request.POST.get('vxt_department_aid')
music_accompaniment = request.POST.get('music_accompaniment')
target_audience = request.POST.get('target_audience')
number_of_participants = request.POST.get('number_of_participants')
achievement = request.POST.get('achievement')
level = request.POST.get('level')
engagement = request.POST.get('engagement')
plan_upplan = request.POST.get('plan_upplan')
created_by = request.POST.get('created_by')
created_date = request.POST.get('created_date')
last_edition_date = request.POST.get('last_edition_date')


"""
