from django.contrib import admin

from .models import Event
admin.site.register(Event)
from .models import Users
admin.site.register(Users)

# Register your models here.
