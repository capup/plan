# Generated by Django 2.2 on 2020-01-26 22:57

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('month', models.CharField(blank=True, default='Січень', max_length=255, null=True, verbose_name='Місяць')),
                ('name_event', models.CharField(blank=True, default='', max_length=255, null=True, verbose_name='Назва')),
                ('status', models.CharField(blank=True, choices=[('actually', 'Актуально'), ('canceled', 'Відмінено'), ('not_specified', 'Не визначено')], default=('actually', 'Актуально'), max_length=255, null=True, verbose_name='Статус')),
                ('day_start', models.DateField(blank=True, default=datetime.datetime(2020, 1, 26, 22, 57, 31, 517404, tzinfo=utc), null=True, verbose_name='Дата початку')),
                ('time_start', models.TimeField(blank=True, default='10:00', null=True, verbose_name='Час початку')),
                ('day_end', models.DateField(default=datetime.datetime(2020, 1, 26, 22, 57, 31, 517404, tzinfo=utc), verbose_name='Дата закінчення')),
                ('time_end', models.TimeField(default='10:00', verbose_name='Час закінчення')),
                ('place_event', models.CharField(blank=True, default='КПДЮ', max_length=255, null=True, verbose_name='Місце')),
                ('type_event', models.CharField(blank=True, choices=[('event', 'Захід'), ('methodical_work', 'Методична робота'), ('sports', 'Спортивне змагання'), ('exhibition', 'Виставка'), ('rehearsal', 'Репетиція')], default=('event', 'Захід'), max_length=255, null=True, verbose_name='Тип')),
                ('direct_event', models.CharField(blank=True, choices=[('bibliographic', 'Бiблiотечно - бiблiографiчний'), ('vіyskovo_patriotic', 'Військово - патріотичний'), ('humanitarian', 'Гуманітарний'), ('doslidnitsko_experimental', 'Дослідницько - експериментальний'), ('ecological_natural', 'Еколого - натуралістичний'), ('mainly_trenuvalny_zbіr', 'Навчально - тренувальний збір'), ('naukovo_technical', 'Науково - технічний'), ('national_patriotic', 'Національно - патріотичний'), ('wellness', 'Оздоровчий'), ('socially_rehabilitation', 'Соціально - реабiлiтацiйний'), ('turistsko_kraznavchiy', 'Туристсько - краєзнавчий'), ('culturally_sports', 'Фізкультурно - спортивний або спортивний'), ('hudozhno_aesthetic', 'Художньо - естетичний')], default=('bibliographic', 'Бiблiотечно - бiблiографiчний'), max_length=255, null=True, verbose_name='Напрям')),
                ('department', models.CharField(blank=True, choices=[('Авіаційно-космічний центр', 'Авіаційно-космічний центр'), ('Відділ біології', 'Відділ біології'), ('Відділ народної творчості', 'Відділ народної творчості'), ('Відділ соціальних ініціатив і партнерства', 'Відділ соціальних ініціатив і партнерства'), ('Відділ туризму та краєзнавства', 'Відділ туризму та краєзнавства'), ('Відділ художньої творчості', 'Відділ художньої творчості'), ('Відділ івент-менеджменту та арт-проектів', 'Відділ івент-менеджменту та арт-проектів'), ('Навчально-методичний відділ', 'Навчально-методичний відділ'), ('Спортивний відділ', 'Спортивний відділ'), ('Інформаційно-творче агентство “ЮН-ПРЕС”', 'Інформаційно-творче агентство “ЮН-ПРЕС”'), ('Відділ STEM-освіти', 'Відділ STEM-освіти')], default='', max_length=255, null=True, verbose_name='Відділ')),
                ('partner', models.CharField(blank=True, default='', max_length=255, null=True, verbose_name='Партнери')),
                ('responsible_person', models.CharField(blank=True, default='', max_length=255, null=True, verbose_name='Відповідальний')),
                ('national_patriotic_event', models.BooleanField(blank=True, default=False, verbose_name='Національно-патриотичне виховання')),
                ('imap_department_aid', models.BooleanField(blank=True, default=False, verbose_name='Допомога ІМАП')),
                ('vxt_department_aid', models.BooleanField(blank=True, default=False, verbose_name='Допомога ВХТ')),
                ('music_accompaniment', models.BooleanField(blank=True, default=False, verbose_name='Музичний супровід')),
                ('target_audience', models.CharField(blank=True, default='', max_length=255, null=True, verbose_name='Цільова аудиторія')),
                ('number_of_participants', models.IntegerField(blank=True, default=1, null=True, verbose_name='Кількість учасників')),
                ('achievement', models.TextField(blank=True, default='', max_length=255, null=True, verbose_name='Досягнення')),
                ('level', models.CharField(blank=True, choices=[('Палацу', 'Палацу'), ('Міський', 'Міський'), ('Всеукраїнський', 'Всеукраїнський'), ('Міжнародний', 'Міжнародний')], default=('Палацу', 'Палацу'), max_length=255, null=True, verbose_name='Рівень')),
                ('engagement', models.CharField(blank=True, choices=[('Організація', 'Організація'), ('Участь', 'Участь'), ('Допомога в організації', 'Допомога в організації')], default=('Організація', 'Організація'), max_length=255, null=True, verbose_name='Роль')),
                ('plan_unplan', models.CharField(blank=True, choices=[('Плановий', 'Плановий'), ('Позаплановий', 'Позаплановий')], default=('Плановий', 'Плановий'), max_length=255, verbose_name='Плановий')),
                ('created_by', models.CharField(blank=True, default='', max_length=255, null=True, verbose_name='Створив')),
                ('created_date', models.DateField(blank=True, default=datetime.datetime(2020, 1, 26, 22, 57, 31, 517404, tzinfo=utc), null=True, verbose_name='Дата створення')),
                ('last_edition_date', models.DateField(blank=True, default=datetime.datetime(2020, 1, 26, 22, 57, 31, 517404, tzinfo=utc), null=True, verbose_name='Дата останньої редакції')),
            ],
        ),
        migrations.CreateModel(
            name='History',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data_time', models.DateTimeField(blank=True, default=datetime.datetime(2020, 1, 26, 22, 57, 31, 519404, tzinfo=utc), null=True, verbose_name='Дата')),
                ('login', models.CharField(blank=True, default='', max_length=255, null=True, verbose_name='Логін')),
                ('activity', models.CharField(blank=True, default='', max_length=255, null=True, verbose_name='Дія')),
            ],
        ),
        migrations.CreateModel(
            name='Users',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default='', max_length=255, null=True, verbose_name='І`мя')),
                ('surname', models.CharField(blank=True, default='', max_length=255, null=True, verbose_name='Прізвище')),
                ('phone', models.CharField(blank=True, default='', max_length=255, null=True, verbose_name='Телефон')),
                ('email', models.CharField(blank=True, default='', max_length=255, null=True, verbose_name='email')),
                ('login', models.CharField(blank=True, default='', max_length=255, null=True, verbose_name='Логін')),
                ('password', models.CharField(blank=True, default='', max_length=255, null=True, verbose_name='Пароль')),
                ('department', models.CharField(blank=True, choices=[('Авіаційно-космічний центр', 'Авіаційно-космічний центр'), ('Відділ біології', 'Відділ біології'), ('Відділ народної творчості', 'Відділ народної творчості'), ('Відділ соціальних ініціатив і партнерства', 'Відділ соціальних ініціатив і партнерства'), ('Відділ туризму та краєзнавства', 'Відділ туризму та краєзнавства'), ('Відділ художньої творчості', 'Відділ художньої творчості'), ('Відділ івент-менеджменту та арт-проектів', 'Відділ івент-менеджменту та арт-проектів'), ('Навчально-методичний відділ', 'Навчально-методичний відділ'), ('Спортивний відділ', 'Спортивний відділ'), ('Інформаційно-творче агентство “ЮН-ПРЕС”', 'Інформаційно-творче агентство “ЮН-ПРЕС”'), ('Відділ STEM-освіти', 'Відділ STEM-освіти')], default='', max_length=255, null=True, verbose_name='Відділ')),
                ('date_registration', models.DateTimeField(blank=True, default=datetime.datetime(2020, 1, 26, 22, 57, 31, 518404, tzinfo=utc), null=True, verbose_name='Дата реєстрації')),
                ('date_last_enter', models.DateTimeField(blank=True, default=datetime.datetime(2020, 1, 26, 22, 57, 31, 518404, tzinfo=utc), null=True, verbose_name='Останній вхід')),
            ],
        ),
    ]
