from django import forms
from .models import Users, Event
from django.core.exceptions import ValidationError

class RegistrationForm(forms.ModelForm):

    """
    def clean_login(self):
        new_login = self.cleaned_data['login'].lower()
        if new_login == 'create':
            raise ValidationError('Не може бути create')
        return new_login
    """

    class Meta:
        model = Users
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Ваше і`мя'}),
            'surname': forms.TextInput(attrs={'placeholder': 'Ваше прізвище','class':'form-control'}),
            'phone': forms.TextInput(attrs={'placeholder': 'У форматі 0987654321'}),
        }

        #title.widget.attrs.update({'class': 'form-control'})

        # exclude = ['id']


class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        name_event = forms.CharField(max_length=50)
        fields = '__all__'
        exclude = ['created_by', 'created_date', 'month']
        widgets = {
            # 'imap_department_aid':forms.BooleanField

        }
